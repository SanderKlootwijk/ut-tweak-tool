# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.5] (Xenial) - 2018-12-05
### Added
- SSH settings: toggle for enabling/disabling SSH.
- Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Centralised app version number.

## [0.4.4] (Xenial)
- Updated the German translation

## [0.4.3] (Xenial)
- Updated the scale option
- Fix the sound notification
- Other small bugs fixed
- Updating translations

## [0.4.2] (Xenial)
- Few bug fixed
- Updating translations
- Adding support to change the SMS tone

## [0.4.1] (Xenial)
- Adding support for Morph Browser

## [0.3.91]
- Fixed issue with Unity8 settings visibility

## [0.3.90]
- Fix charset when reading .desktop files, thanks to Michael Zanetti
- [OTA-14] Added option to enable/disable the indicator menu, thanks to Brian Douglass
- [OTA-14] Added option to enable/disable the launcher, thanks to Brian Douglass
- Re-sorted entries for Unity 8 settings
- Updated translation template
